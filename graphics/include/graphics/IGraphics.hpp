//
// IGraphics.h
//
//  Created on: 2018. �pr. 24.
//      Author: fasz
//

#ifndef GRAPHICS_IGRAPHICS_H_
#define GRAPHICS_IGRAPHICS_H_

#include <common/geometry.hpp>

#include <vector>
#include <cstdint>

namespace gurgula
{

namespace graphics
{
typedef int ScreenUnit;
typedef vector<ScreenUnit> ScreenCoordinate;
typedef Rectangle<ScreenCoordinate> Box;
typedef std::vector<ScreenCoordinate> Polygon;

class IGraphics
{
public:
  struct Color
  {
    uint8_t red, green, blue;
  };

  virtual ScreenCoordinate size() const = 0;

  virtual void drawBox(const Box& box, Color color, bool filled = false) const = 0;
  virtual void drawPolygon(const Polygon& pol,
    ScreenCoordinate position,
    Degree rotate,
    float scale,
    Color color,
    bool filled = false) const = 0;

  virtual ~IGraphics()
  {
  }
};

} // namespace graphics

} // namespace gurgula

#endif // GRAPHICS_IGRAPHICS_H_
