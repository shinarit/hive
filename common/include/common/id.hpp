//
// id.hpp
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#ifndef HC50792B3_7988_4CC2_BF00_3F0194AEAF6B
#define HC50792B3_7988_4CC2_BF00_3F0194AEAF6B

namespace gurgula
{

class Id
{
public:
  bool operator==(Id lhs) const
  {
    return mValue == lhs.mValue;
  }

private:
  typedef int BaseType;
  Id(BaseType value): mValue(value)
  {
  }
  BaseType mValue;
  friend class IdGenerator;
  friend class ::std::hash<Id>;
};

class IdGenerator
{
public:
  IdGenerator(): mToken(0)
  {
  }
  Id nextId()
  {
    return mToken++;
  }

private:
  typedef Id::BaseType BaseType;
  BaseType mToken;
};

} // namespace gurgula

namespace std
{
template<>
struct hash<::gurgula::Id>
{
  size_t operator()(::gurgula::Id id) const
  {
    return id.mValue;
  }
};
} // namespace std

#endif // HC50792B3_7988_4CC2_BF00_3F0194AEAF6B
