#include <common/geometry.hpp>
#include <ui/GlWindow.hpp>
#include <common/Logger.hpp>
#include <ui/GameWindow.hpp>

#include <gurgulaConfig.h>

#include <chrono>
#include <thread>

namespace
{
std::chrono::milliseconds UPDATE_FREQUENCY(10);
}

int main(int argc, char** argv)
{
  const ::gurgula::ui::GameConfig::InsectSet DefaultInsectSet = {{::gurgula::ui::Insect::Species::QUEEN_BEE, 1},
    {::gurgula::ui::Insect::Species::SPIDER, 4},
    {::gurgula::ui::Insect::Species::GRASSHOPPER, 1},
    {::gurgula::ui::Insect::Species::BEETLE, 1}};
  ::std::thread loggerThread(::gurgula::logger::Logger::flusher);
  {
    const int major(gurgula_VERSION_MAJOR);
    const int minor(gurgula_VERSION_MINOR);
    ::gurgula::ui::GlWindow glWindow(::gurgula::ui::IPhysicalWindow::Mode::BORDERED_WINDOW, {1400, 1000});
    ::gurgula::ui::GameWindow gameWindow(glWindow, glWindow, {DefaultInsectSet, ::gurgula::ui::Insect::Side::BLACK});

    const ::std::chrono::milliseconds frameUpdateDelay(UPDATE_FREQUENCY);
    auto previousTime(::std::chrono::steady_clock::now());
    bool doubleDraw(false);
    while (!glWindow.wantsToDie())
    {
      glWindow.handleEvents();
      auto currentTime(::std::chrono::steady_clock::now());
      if (currentTime - previousTime >= frameUpdateDelay)
      {
        glWindow.startUpdate();
        bool updateRequired(gameWindow.graphicsUpdateNeeded());
        if (doubleDraw || updateRequired)
        {
          gameWindow.updateGraphics();
          doubleDraw = updateRequired;
        }
        glWindow.finishUpdate();
        previousTime = currentTime;
      }
    }
  }
  ::gurgula::logger::Logger::stop();
  loggerThread.join();
}
