//
// IPhysicalWindow.h
//
//  Created on: 2018. �pr. 28.
//      Author: fasz
//

#ifndef GRAPHICS_INCLUDE_GRAPHICS_IPHYSICALWINDOW_HPP_
#define GRAPHICS_INCLUDE_GRAPHICS_IPHYSICALWINDOW_HPP_

#include <common/geometry.hpp>

namespace gurgula
{
namespace ui
{
class IPhysicalWindow
{
public:
  class PhysicalWindowException
  {
  };
  enum class Mode
  {
    BORDERED_WINDOW,
    BORDERLESS_WINDOW,
    EXCLUSIVE_FULLSCREEN
  };

  virtual bool wantsToDie() const = 0;

  virtual Mode getMode() const = 0;
  virtual bool changeMode(Mode mode) = 0;

  typedef int WindowUnit;
  typedef vector<WindowUnit> WindowCoordinate;

  virtual WindowCoordinate getSize() const = 0;
  virtual bool setSize(WindowCoordinate size) = 0;

  virtual void startUpdate() = 0;
  virtual void finishUpdate() = 0;

  virtual ~IPhysicalWindow()
  {
  }
};
} // namespace ui
} // namespace gurgula

#endif // GRAPHICS_INCLUDE_GRAPHICS_IPHYSICALWINDOW_HPP_
