//
// IObserver.hpp
//
//  Created on: 2018. m�j. 26.
//      Author: fasz
//

#ifndef HB7953BAC_F73A_4781_A564_114ACE664A5F
#define HB7953BAC_F73A_4781_A564_114ACE664A5F

#include <common/geometry.hpp>

namespace gurgula
{
namespace ui
{

class IObserver
{
public:
  typedef float Scale;
  typedef vector<int> GameCoordinate;
  struct Position
  {
    GameCoordinate center;
    Scale scale;
  };

  virtual Position getCurrentPosition() const = 0;
};

} // namespace ui
} // namespace gurgula

#endif // HB7953BAC_F73A_4781_A564_114ACE664A5F
