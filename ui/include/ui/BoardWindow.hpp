//
// BoardWindow.hpp
//
//  Created on: 2018. aug. 29.
//      Author: fasz
//

#ifndef H1213A5D5_A323_4C63_9119_AA3B33DA9E48
#define H1213A5D5_A323_4C63_9119_AA3B33DA9E48

#include <ui/IWindow.hpp>

#include <ui/BugCommon.hpp>
#include <ui/Insect.hpp>
#include <ui/Animation.hpp>
#include <common/Logger.hpp>

namespace gurgula
{
namespace ui
{
class BoardWindow : public IWindow
{
public:
  BoardWindow(IWindow& parent, const graphics::Box& boundingBox);

  void updateGraphics(const GameBoard& board,
    ::std::optional<HexCoordinate> selection,
    Insect::Targets selectionTargets,
    ::std::optional<HexCoordinate> secondarySelection,
    Insect::Targets secondaryTargets,
    bool liftStacks);

  HexCoordinate boardCoordinates(graphics::ScreenCoordinate coord);

private:
  struct Cube
  {
    double x;
    double y;
    double z;
  };
  typedef gurgula::vector<float> PreciseCoordinate;

  virtual bool checkForGraphicsChange() override;

  PreciseCoordinate cubeToFlatHex(Cube cube);
  Cube cubeRound(double x, double y, double z);
  graphics::ScreenCoordinate tilePlacement(HexCoordinate coord);

  double mCurrentZoom;
  PreciseCoordinate mCurrentCenter;
  Animation mStackingAnimation;

  logger::Logger mLogger;
};

} // namespace ui
} // namespace gurgula

#endif // H1213A5D5_A323_4C63_9119_AA3B33DA9E48
