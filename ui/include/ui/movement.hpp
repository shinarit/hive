//
// movement.hpp
//
//  Created on: 2018. nov. 30.
//      Author: fasz
//

#ifndef HA045806F_6BAA_4CAD_92AA_F722030F10DC
#define HA045806F_6BAA_4CAD_92AA_F722030F10DC

#include <ui/Insect.hpp>

namespace gurgula
{
namespace ui
{

bool fulfillsOneHive(const HexCoordinate position, const GameBoard& board);

::std::vector<Insect::Path> queenBeePaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> spiderPaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> beetlePaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> grasshopperPaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> soldierAntPaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> mosquitoPaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> ladybugPaths(const HexCoordinate position, const GameBoard& board);
::std::vector<Insect::Path> pillBugPaths(const HexCoordinate position, const GameBoard& board);

} // namespace ui
} // namespace gurgula

#endif // HA045806F_6BAA_4CAD_92AA_F722030F10DC
