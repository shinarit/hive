//
// BugCommon.hpp
//
//  Created on: 2018. aug. 23.
//      Author: fasz
//

#ifndef H534623DC_000B_4E6E_9B39_ED4DE92FB586
#define H534623DC_000B_4E6E_9B39_ED4DE92FB586

#include <common/geometry.hpp>
#include <graphics/IGraphics.hpp>

#include <map>
#include <array>
#include <vector>

namespace gurgula
{
namespace ui
{
class Insect;
typedef vector<int> HexCoordinate;

struct DrawOrder
{
  bool operator()(const HexCoordinate& lhs, const HexCoordinate& rhs) const
  {
    return lhs.x < rhs.x || lhs.x == rhs.x && lhs.y > rhs.y;
  }
};
typedef ::std::map<HexCoordinate, ::std::vector<Insect>, DrawOrder> GameBoard;

const graphics::Polygon BaseHex{{50, -87}, {-50, -87}, {-100, 0}, {-50, 87}, {50, 87}, {100, 0}};
const double BASE_HEX_SIZE = 100;
const double BASE_HEX_HORIZONTAL_DISTANCE = 1.5 * BASE_HEX_SIZE;
const double BASE_HEX_HORIZONTAL_TRIM = 1. / 6;
const double BASE_HEX_VERTICAL_DISTANCE = 1.7320508075688772935274463415059 * BASE_HEX_SIZE;
typedef ::std::array<HexCoordinate, 6> Neighbours;
// always return in clockwise order, starting from the top
Neighbours getNeighbours(HexCoordinate coord);
enum class Direction
{
  N,
  NE,
  SE,
  S,
  SW,
  NW
};

const graphics::IGraphics::Color WHITE_PIECE_BASE_COLOR = {255, 255, 255};
const graphics::IGraphics::Color BLACK_PIECE_BASE_COLOR = {100, 100, 100};

} // namespace ui
} // namespace gurgula

#endif // H534623DC_000B_4E6E_9B39_ED4DE92FB586
