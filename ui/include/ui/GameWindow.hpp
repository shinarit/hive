//
// GameWindow.hpp
//
//  Created on: 2018. aug. 22.
//      Author: fasz
//

#ifndef HE4ECC12C_9D28_4E39_90B8_14B8F4374668
#define HE4ECC12C_9D28_4E39_90B8_14B8F4374668

#include <ui/IWindow.hpp>

#include <ui/IEventDistributor.hpp>
#include <ui/GameConfig.hpp>
#include <ui/PlayerWindow.hpp>
#include <ui/BoardWindow.hpp>
#include <ui/Insect.hpp>
#include <ui/BugCommon.hpp>

#include <common/Logger.hpp>

#include <optional>

namespace gurgula
{
namespace ui
{
class GameWindow : public IWindow
{
public:
  GameWindow(graphics::IGraphics& graphics, IEventDistributor& eventDistributor, GameConfig config);

  void updateGraphics();

private:
  static const int LATEST_TURN_TO_PLACE_QUEEN = 3;

  enum class Step
  {
    BLACK_STEP,
    WHITE_STEP,
    BLACK_WIN,
    WHITE_WIN,
    DRAW
  };

  virtual bool checkForGraphicsChange() override;

  virtual void drawBox(const graphics::Box& box, graphics::IGraphics::Color color, bool filled = false);
  virtual void drawPolygon(const graphics::Polygon& pol,
    graphics::ScreenCoordinate position,
    Degree rotate,
    float scale,
    graphics::IGraphics::Color color,
    bool filled = false);

  void mouseButtonCallback(const IEventDistributor::MousePressedEvent& event);
  void mouseMoveCallback(const IEventDistributor::MouseMovedEvent& event);
  void keyboardCallback(const IEventDistributor::KeyboardEvent& event);
  void stackLiftKeyCallback(const IEventDistributor::KeyboardEvent& event);

  bool canMove(HexCoordinate to) const;
  bool canPlace(HexCoordinate to) const;
  void checkForWinconditions();

  void saveState() const;
  void loadState();

  void switchSide();
  Insect::Side sideFromStep(Step step) const;
  Step stepFromSide(Insect::Side side) const;
  bool endOfGame(Step step) const;

  graphics::IGraphics& mGraphics;

  graphics::ScreenCoordinate mMousePosition;
  GameBoard mBoard;

  PlayerWindow mWhiteSet;
  PlayerWindow mBlackSet;
  BoardWindow mBoardWindow;

  logger::Logger mGameLogger;

  struct Selection
  {
    enum class Occasion
    {
      PLACEMENT_FROM_WHITE,
      PLACEMENT_FROM_BLACK,
      TILE_ON_BOARD
    };

    // over reserve
    Selection(Insect insect, Insect::Side hoveredSide)
      : selectedInsect(insect)
      , occasion(Insect::Side::BLACK == hoveredSide ? Occasion::PLACEMENT_FROM_BLACK : Occasion::PLACEMENT_FROM_WHITE)
    {
    }
    // over insect
    Selection(Insect insect, GameBoard::key_type coordinate)
      : selectedInsect(insect), occasion(Occasion::TILE_ON_BOARD), coordinate(coordinate)
    {
    }
    // over empty space
    Selection(GameBoard::key_type coordinate): occasion(Occasion::TILE_ON_BOARD), coordinate(coordinate)
    {
    }

    ::std::optional<Insect> selectedInsect;
    Occasion occasion;
    ::std::optional<GameBoard::key_type> coordinate;
  };
  ::std::optional<Selection> mCurrentSelection;
  ::std::optional<Selection> mCurrentHover;
  Step mCurrentStep;
  int mTurnCounter = 0;
  bool mQueenPlaced[2] = {false, false};

  Insect::Targets mSelectionTargets;
  Insect::Targets mHoverTargets;

  bool mStacksLifted = false;
};

} // namespace ui
} // namespace gurgula

#endif // HE4ECC12C_9D28_4E39_90B8_14B8F4374668
