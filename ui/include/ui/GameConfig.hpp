//
// GameSet.hpp
//
//  Created on: 2018. aug. 22.
//      Author: fasz
//

#ifndef HE0F98E6C_A798_4860_9C2B_EDF393F7FACC
#define HE0F98E6C_A798_4860_9C2B_EDF393F7FACC

#include <ui/Insect.hpp>

#include <map>

namespace gurgula
{
namespace ui
{

struct GameConfig
{
  typedef ::std::map<Insect::Species, int> InsectSet;
  InsectSet insects;
  Insect::Side startingPlayer;
};

} // namespace ui
} // namespace gurgula

#endif // HE0F98E6C_A798_4860_9C2B_EDF393F7FACC
