//
// animation.hpp
//
//  Created on: 2018. dec. 3.
//      Author: fasz
//

#ifndef H5906DF10_CB3D_4E49_8948_824DD076697F
#define H5906DF10_CB3D_4E49_8948_824DD076697F

#include <chrono>

namespace gurgula
{
namespace ui
{

class Animation
{
public:
  enum class State
  {
    AT_START,
    GOING,
    GOING_BACKWARDS,
    AT_END
  };
  // the thing will always be [0,1]
  typedef double AnimationValue;
  typedef ::std::chrono::milliseconds Time;

  Animation(Time length);

  bool ongoing();
  AnimationValue currentValue() const;
  State currentState() const;

  void reStart();
  void reverse();

private:
  AnimationValue calculateValue() const;
  void switchDirection();

  State mCurrentState;
  const Time mLength;
  ::std::chrono::steady_clock::time_point mStartTime;
  ::std::chrono::steady_clock::time_point mEndTime;
};

} // namespace ui
} // namespace gurgula

#endif // H5906DF10_CB3D_4E49_8948_824DD076697F
