//
// Insect.hpp
//
//  Created on: 2018. aug. 22.
//      Author: fasz
//

#ifndef HCC8DC840_4CA2_476B_8804_11D5A138095E
#define HCC8DC840_4CA2_476B_8804_11D5A138095E

#include <ui/BugCommon.hpp>

#include <vector>
#include <set>
#include <iosfwd>

namespace gurgula
{
namespace ui
{

struct Insect
{
  enum class Side
  {
    WHITE,
    BLACK
  } side;
  enum class Species
  {
    QUEEN_BEE,
    SPIDER,
    BEETLE,
    GRASSHOPPER,
    SOLDIER_ANT,
    // expansion stuff
    MOSQUITO,
    LADYBUG,
    PILL_BUG
  } species;

  struct BugShape
  {
    BugShape(::std::istream& source);
    struct Piece
    {
      graphics::Polygon polygon;
      enum
      {
        FILL,
        OUTLINE,
        BACKGROUND
      } mode;
    };
    typedef ::std::vector<Piece> Pieces;

    graphics::IGraphics::Color color;
    Pieces pieces;
  };
  BugShape getShape() const;
  typedef ::std::vector<HexCoordinate> Path;
  ::std::vector<Path> getPaths(const HexCoordinate position, const GameBoard& board) const;
  static bool canPlace(Side side, HexCoordinate to, const GameBoard& board);
  typedef ::std::set<GameBoard::key_type> Targets;
  static Targets collectPlacementTargets(Side side, const GameBoard& board);
};

Insect produceInsect(Insect::Side side, Insect::Species species);

const char* nameSpecies(Insect::Species species);

::std::ostream& operator<<(::std::ostream& out, const Insect& insect);

bool operator==(const Insect& lhs, const Insect& rhs);

const double PIECE_SIZE_RATION = 0.8;

} // namespace ui
} // namespace gurgula

#endif // HCC8DC840_4CA2_476B_8804_11D5A138095E
