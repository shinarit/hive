//
// IWindow.h
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#ifndef UI_INCLUDE_UI_IWINDOW_HPP_
#define UI_INCLUDE_UI_IWINDOW_HPP_

#include <common/geometry.hpp>
#include <graphics/IGraphics.hpp>

namespace gurgula
{
namespace ui
{

class IWindow
{
public:
  IWindow(IWindow& parent, const graphics::Box& boundingBox);
  virtual ~IWindow()
  {
  }

  graphics::ScreenCoordinate translatePosition(graphics::ScreenCoordinate coord);
  graphics::ScreenCoordinate translatePosition(graphics::ScreenCoordinate coord, bool& inWindow);

  bool graphicsUpdateNeeded();

protected:
  virtual void drawBox(const graphics::Box& box, graphics::IGraphics::Color color, bool filled = false);
  virtual void drawPolygon(const graphics::Polygon& pol,
    graphics::ScreenCoordinate position,
    Degree rotate,
    float scale,
    graphics::IGraphics::Color color,
    bool filled = false);

  virtual bool checkForGraphicsChange();

  graphics::ScreenCoordinate size() const;
  void dirty();
  void clean();

  const graphics::Box mBoundingBox;

private:
  bool mDirty = true;
  IWindow& mParent;
};

} // namespace ui
} // namespace gurgula

#endif // UI_INCLUDE_UI_IWINDOW_HPP_
