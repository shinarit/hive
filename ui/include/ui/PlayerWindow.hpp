//
// PlayerWindow.hpp
//
//  Created on: 2018. aug. 22.
//      Author: fasz
//

#ifndef HDB035126_4FA7_4710_A12B_C90213BF4B24
#define HDB035126_4FA7_4710_A12B_C90213BF4B24

#include <ui/IWindow.hpp>

#include <ui/Insect.hpp>
#include <ui/GameConfig.hpp>
#include <common/Logger.hpp>

#include <vector>
#include <fstream>

namespace gurgula
{
namespace ui
{

class PlayerWindow : public IWindow
{
public:
  PlayerWindow(IWindow& parent, const graphics::Box& boundingBox, Insect::Side side, GameConfig::InsectSet set);

  void updateGraphics();

  const Insect* hover(graphics::ScreenCoordinate coord) const;
  void removeInsect(Insect insect);

private:
  friend class GameWindow;

  graphics::ScreenCoordinate positionForIndex(int i) const;

  void save(::std::ofstream& out) const;
  void load(::std::ifstream& in);

  struct Piece
  {
    Insect insect;
    int remaining;
  };
  const graphics::IGraphics::Color mPieceBaseColor;
  const graphics::IGraphics::Color mBackColor;
  typedef ::std::vector<Piece> RemainingPieces;
  RemainingPieces mRemainingInsects;

  bool mNeedUpdate = false;

  logger::Logger mLogger;
};

} // namespace ui
} // namespace gurgula

#endif // HDB035126_4FA7_4710_A12B_C90213BF4B24
