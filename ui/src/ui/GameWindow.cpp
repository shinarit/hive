//
// GameWindow.cpp
//
//  Created on: 2018. aug. 22.
//      Author: fasz
//

#include <ui/GameWindow.hpp>

#include <fstream>

namespace gurgula
{
namespace ui
{

namespace
{
const char* const TEMP_SAVE_FILE_NAME = "mentett allas.sav";
} // namespace

const int PlayerWindowWidth = 200;

GameWindow::GameWindow(graphics::IGraphics& graphics, IEventDistributor& eventDistributor, GameConfig config)
  : IWindow(*this, {{0, 0}, {graphics.size().x, graphics.size().y}})
  , mGraphics(graphics)
  , mWhiteSet(*this, {{0, 0}, {PlayerWindowWidth, size().y}}, Insect::Side::WHITE, config.insects)
  , mBlackSet(*this, {{size().x - PlayerWindowWidth, 0}, {size().x, size().y}}, Insect::Side::BLACK, config.insects)
  , mBoardWindow(*this, {{PlayerWindowWidth, 0}, {size().x - PlayerWindowWidth, size().y}})
  , mGameLogger("GAME")
  , mCurrentStep(stepFromSide(config.startingPlayer))
{
  eventDistributor.registerMouseButtonEventHandler(
    IEventDistributor::LEFT_MOUSE_BUTTON, ::std::bind(&mouseButtonCallback, this, ::std::placeholders::_1));
  eventDistributor.registerMouseButtonEventHandler(
    IEventDistributor::RIGHT_MOUSE_BUTTON, ::std::bind(&mouseButtonCallback, this, ::std::placeholders::_1));
  eventDistributor.registerMouseButtonEventHandler(
    IEventDistributor::MIDDLE_MOUSE_BUTTON, ::std::bind(&mouseButtonCallback, this, ::std::placeholders::_1));
  eventDistributor.registerMouseMoveEventHandler(::std::bind(&mouseMoveCallback, this, ::std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(
    IEventDistributor::NamedCodes::S, ::std::bind(&keyboardCallback, this, ::std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(
    IEventDistributor::NamedCodes::L, ::std::bind(&keyboardCallback, this, ::std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(
    IEventDistributor::NamedCodes::K, ::std::bind(&stackLiftKeyCallback, this, ::std::placeholders::_1));
}

void GameWindow::updateGraphics()
{
  mWhiteSet.updateGraphics();
  mBlackSet.updateGraphics();
  ::std::optional<HexCoordinate> selection;
  if (mCurrentSelection.has_value() && Selection::Occasion::TILE_ON_BOARD == mCurrentSelection->occasion)
  {
    selection = *mCurrentSelection->coordinate;
  }
  ::std::optional<HexCoordinate> hover;
  if (mCurrentHover.has_value() && Selection::Occasion::TILE_ON_BOARD == mCurrentHover->occasion
    && mCurrentHover->selectedInsect.has_value())
  {
    hover = *mCurrentHover->coordinate;
  }
  mBoardWindow.updateGraphics(mBoard, selection, mSelectionTargets, hover, mHoverTargets, mStacksLifted);
  drawBox({{mMousePosition.x, mMousePosition.y}, {mMousePosition.x + 100, mMousePosition.y + 100}}, {255, 0, 0});

  clean();
}

bool GameWindow::checkForGraphicsChange()
{
  return mWhiteSet.graphicsUpdateNeeded() || mBlackSet.graphicsUpdateNeeded() || mBoardWindow.graphicsUpdateNeeded();
}

void GameWindow::drawBox(const graphics::Box& box, graphics::IGraphics::Color color, bool filled)
{
  mGraphics.drawBox(box, color, filled);
}

void GameWindow::drawPolygon(const graphics::Polygon& pol,
  graphics::ScreenCoordinate position,
  Degree rotate,
  float scale,
  graphics::IGraphics::Color color,
  bool filled)
{
  mGraphics.drawPolygon(pol, position, rotate, scale, color, filled);
}

void GameWindow::mouseButtonCallback(const IEventDistributor::MousePressedEvent& event)
{
  if (!endOfGame(mCurrentStep))
  {
    if (IEventDistributor::LEFT_MOUSE_BUTTON == event.button
      && IEventDistributor::ButtonStatus::RELEASED == event.status)
    {
      if (mCurrentSelection.has_value() && mCurrentHover.has_value()
        && Selection::Occasion::TILE_ON_BOARD == mCurrentHover->occasion)
      {
        bool actionHappened(false);
        if (Selection::Occasion::TILE_ON_BOARD == mCurrentSelection->occasion
          && canMove(mCurrentHover->coordinate.value())
          && mQueenPlaced[static_cast<int>(mCurrentSelection->selectedInsect->side)])
        {
          HexCoordinate from(*mCurrentSelection->coordinate);
          HexCoordinate to(*mCurrentHover->coordinate);
          Insect insectToMove(mBoard[from].back());
          mGameLogger() << "insect (" << insectToMove << ") moved to " << to;
          mBoard[to].push_back(insectToMove);
          mBoard[from].pop_back();
          if (mBoard[from].empty())
          {
            mBoard.erase(from);
          }
          actionHappened = true;
        }
        else if (Selection::Occasion::PLACEMENT_FROM_BLACK == mCurrentSelection->occasion
          && canPlace(mCurrentHover->coordinate.value()))
        {
          Insect insectToPlace(mCurrentSelection->selectedInsect.value());
          if (Insect::Species::QUEEN_BEE == insectToPlace.species)
          {
            mQueenPlaced[static_cast<int>(Insect::Side::BLACK)] = true;
          }
          if (mQueenPlaced[static_cast<int>(Insect::Side::BLACK)] || mTurnCounter / 2 < LATEST_TURN_TO_PLACE_QUEEN)
          {
            mGameLogger() << "insect (" << insectToPlace << ") placed on " << mCurrentHover->coordinate.value();
            mBoard[mCurrentHover->coordinate.value()].push_back(insectToPlace);
            mBlackSet.removeInsect(insectToPlace);
            actionHappened = true;
          }
        }
        else if (Selection::Occasion::PLACEMENT_FROM_WHITE == mCurrentSelection->occasion
          && canPlace(mCurrentHover->coordinate.value()))
        {
          Insect insectToPlace(mCurrentSelection->selectedInsect.value());
          if (Insect::Species::QUEEN_BEE == insectToPlace.species)
          {
            mQueenPlaced[static_cast<int>(Insect::Side::WHITE)] = true;
          }
          if (mQueenPlaced[static_cast<int>(Insect::Side::WHITE)] || mTurnCounter / 2 < LATEST_TURN_TO_PLACE_QUEEN)
          {
            mGameLogger() << "insect (" << insectToPlace << ") placed on " << mCurrentHover->coordinate.value();
            mBoard[mCurrentHover->coordinate.value()].push_back(insectToPlace);
            mWhiteSet.removeInsect(insectToPlace);
            actionHappened = true;
          }
        }

        if (actionHappened)
        {
          mCurrentSelection.reset();
          switchSide();
          ++mTurnCounter;
          mSelectionTargets.clear();
          checkForWinconditions();

          dirty();
        }
      }
      else if (!mCurrentSelection.has_value() && mCurrentHover.has_value())
      {
        if (Step::BLACK_STEP == mCurrentStep && Selection::Occasion::PLACEMENT_FROM_BLACK == mCurrentHover->occasion)
        {
          mGameLogger() << "insect PICKED from RESERVE: " << mCurrentHover->selectedInsect.value();
          //        mCurrentSelection.emplace(mCurrentHover->selectedInsect.value(), Insect::Side::BLACK);
          mCurrentSelection = mCurrentHover;
          mSelectionTargets = Insect::collectPlacementTargets(Insect::Side::BLACK, mBoard);

          dirty();
        }
        else if (Step::WHITE_STEP == mCurrentStep
          && Selection::Occasion::PLACEMENT_FROM_WHITE == mCurrentHover->occasion)
        {
          mGameLogger() << "insect PICKED from RESERVE: " << mCurrentHover->selectedInsect.value();
          mCurrentSelection.emplace(mCurrentHover->selectedInsect.value(), Insect::Side::WHITE);
          mSelectionTargets = Insect::collectPlacementTargets(Insect::Side::WHITE, mBoard);

          dirty();
        }
        else if (Selection::Occasion::TILE_ON_BOARD == mCurrentHover->occasion
          && mCurrentHover->selectedInsect.has_value()
          && mCurrentHover->selectedInsect->side == sideFromStep(mCurrentStep))
        {
          mGameLogger() << "insect PICKED from BOARD: " << mCurrentHover->selectedInsect.value();
          mCurrentSelection = mCurrentHover;
          mSelectionTargets.clear();
          const auto paths(mCurrentSelection->selectedInsect->getPaths(*mCurrentSelection->coordinate, mBoard));
          for (const auto& path : paths)
          {
            mSelectionTargets.insert(path.back());
          }

          dirty();
        }
      }
    }
    else if (IEventDistributor::RIGHT_MOUSE_BUTTON == event.button
      && IEventDistributor::ButtonStatus::RELEASED == event.status)
    {
      mCurrentSelection.reset();
      mSelectionTargets.clear();

      dirty();
    }
  }
}

void GameWindow::mouseMoveCallback(const IEventDistributor::MouseMovedEvent& event)
{
  const graphics::ScreenCoordinate prevCoord(mMousePosition);
  mMousePosition
    = graphics::ScreenCoordinate{static_cast<int>(event.cursorPosition.x), static_cast<int>(event.cursorPosition.y)};
  if (prevCoord != mMousePosition)
  {
    dirty();
    bool inBoard;
    auto boardCoord(mBoardWindow.translatePosition(mMousePosition, inBoard));
    if (inBoard)
    {
      auto coords(mBoardWindow.boardCoordinates(boardCoord));
      auto it = mBoard.find(coords);
      if (it != mBoard.end())
      {
        mCurrentHover.emplace(it->second.back(), coords);
        mHoverTargets.clear();
        const auto paths(mCurrentHover->selectedInsect->getPaths(*mCurrentHover->coordinate, mBoard));
        for (const auto& path : paths)
        {
          mHoverTargets.insert(path.back());
        }
      }
      else
      {
        mCurrentHover.emplace(coords);
        mHoverTargets.clear();
      }
    }
    else
    {
      const Insect* insect;
      if (insect = mWhiteSet.hover(mWhiteSet.translatePosition(mMousePosition)))
      {
        mCurrentHover.emplace(*insect, Insect::Side::WHITE);
        mHoverTargets = Insect::collectPlacementTargets(Insect::Side::WHITE, mBoard);
      }
      else if (insect = mBlackSet.hover(mBlackSet.translatePosition(mMousePosition)))
      {
        mCurrentHover.emplace(*insect, Insect::Side::BLACK);
        mHoverTargets = Insect::collectPlacementTargets(Insect::Side::BLACK, mBoard);
      }
      else
      {
        mCurrentHover.reset();
      }
    }
  }
}

void GameWindow::keyboardCallback(const IEventDistributor::KeyboardEvent& event)
{
  if (IEventDistributor::NamedCodes::S == event.code)
  {
    saveState();
  }
  else if (IEventDistributor::NamedCodes::L == event.code)
  {
    loadState();
  }
}

void GameWindow::stackLiftKeyCallback(const IEventDistributor::KeyboardEvent& event)
{
  mStacksLifted = IEventDistributor::ButtonStatus::PRESSED == event.status;
  dirty();
}

bool GameWindow::canMove(HexCoordinate to) const
{
  bool can(0 < mSelectionTargets.count(to));
  mGameLogger() << "can MOVE at (" << to << ")? who knwos! " << (can ? "yes" : "no");
  return can;
}

bool GameWindow::canPlace(HexCoordinate to) const
{
  bool can(0 < mSelectionTargets.count(to));
  mGameLogger() << "can PLACE at (" << to << ")? who knwos! " << (can ? "yes" : "no");
  return can;
}

void GameWindow::checkForWinconditions()
{
  bool blackSurrounded(false);
  bool whiteSurrounded(false);
  for (const auto tile : mBoard)
  {
    const auto queen(::std::find_if(begin(tile.second), end(tile.second), [](const auto insect) {
      return Insect::Species::QUEEN_BEE == insect.species;
    }));
    if (queen != end(tile.second))
    {
      const auto neighbours(getNeighbours(tile.first));
      if (::std::all_of(
            begin(neighbours), end(neighbours), [this](const auto coord) { return 0 < mBoard.count(coord); }))
      {
        switch (queen->side)
        {
          case Insect::Side::BLACK:
            blackSurrounded = true;
            break;
          case Insect::Side::WHITE:
            whiteSurrounded = true;
            break;
        }
      }
    }
  }
  if (blackSurrounded && whiteSurrounded)
  {
    mCurrentStep = Step::DRAW;
  }
  else if (blackSurrounded)
  {
    mCurrentStep = Step::WHITE_WIN;
  }
  else if (whiteSurrounded)
  {
    mCurrentStep = Step::BLACK_WIN;
  }
}

template<class T>
void saveSimpleData(const T& data, ::std::ofstream& out)
{
  out.write(reinterpret_cast<const char*>(&data), sizeof(T));
}

template<class T>
void loadSimpleData(T& data, ::std::ifstream& in)
{
  in.read(reinterpret_cast<char*>(&data), sizeof(T));
}

void GameWindow::saveState() const
{
  ::std::ofstream file(TEMP_SAVE_FILE_NAME, ::std::ofstream::binary);

  saveSimpleData(mMousePosition, file);
  saveSimpleData(mCurrentStep, file);
  saveSimpleData(mTurnCounter, file);
  saveSimpleData(mQueenPlaced[0], file);
  saveSimpleData(mQueenPlaced[1], file);
  saveSimpleData(mBoard.size(), file);
  for (const auto tile : mBoard)
  {
    saveSimpleData(tile.first, file);
    saveSimpleData(tile.second.size(), file);
    for (const auto insect : tile.second)
    {
      saveSimpleData(insect, file);
    }
  }
  mWhiteSet.save(file);
  mBlackSet.save(file);
}

void GameWindow::loadState()
{
  ::std::ifstream file(TEMP_SAVE_FILE_NAME, ::std::ifstream::binary);

  loadSimpleData(mMousePosition, file);
  loadSimpleData(mCurrentStep, file);
  loadSimpleData(mTurnCounter, file);
  loadSimpleData(mQueenPlaced[0], file);
  loadSimpleData(mQueenPlaced[1], file);
  GameBoard::size_type newSize;
  loadSimpleData(newSize, file);
  mBoard.clear();
  for (int i(0); i < newSize; ++i)
  {
    GameBoard::key_type newKey;
    loadSimpleData(newKey, file);
    GameBoard::mapped_type::size_type tileSize;
    loadSimpleData(tileSize, file);
    GameBoard::mapped_type newValue;
    newValue.reserve(tileSize);
    for (int i(0); i < tileSize; ++i)
    {
      GameBoard::mapped_type::value_type newInsect;
      loadSimpleData(newInsect, file);
      newValue.push_back(newInsect);
    }
    mBoard.insert({newKey, newValue});
  }
  mWhiteSet.load(file);
  mBlackSet.load(file);
  dirty();
}

void GameWindow::switchSide()
{
  switch (mCurrentStep)
  {
    case Step::BLACK_STEP:
      mCurrentStep = Step::WHITE_STEP;
      break;
    case Step::WHITE_STEP:
      mCurrentStep = Step::BLACK_STEP;
      break;
    default:
      mGameLogger() << "ERROR " << __func__ << ' ' << static_cast<int>(mCurrentStep);
  }
}

Insect::Side GameWindow::sideFromStep(Step step) const
{
  switch (step)
  {
    case Step::BLACK_STEP:
      return Insect::Side::BLACK;
    case Step::WHITE_STEP:
      return Insect::Side::WHITE;
    default:
      mGameLogger() << "ERROR " << __func__ << ' ' << static_cast<int>(mCurrentStep);
  }
}

GameWindow::Step GameWindow::stepFromSide(Insect::Side side) const
{
  return ((Insect::Side::BLACK == side) ? GameWindow::Step::BLACK_STEP : GameWindow::Step::WHITE_STEP);
}

bool GameWindow::endOfGame(Step step) const
{
  switch (step)
  {
    case GameWindow::Step::BLACK_STEP:
    case GameWindow::Step::WHITE_STEP:
      return false;
    case Step::BLACK_WIN:
    case Step::WHITE_WIN:
    case Step::DRAW:
      return true;
  }
}

} // namespace ui
} // namespace gurgula
