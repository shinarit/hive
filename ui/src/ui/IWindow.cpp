//
// IWindow.cpp
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#include <ui/IWindow.hpp>

namespace gurgula
{
namespace ui
{

IWindow::IWindow(IWindow& parent, const graphics::Box& boundingBox): mBoundingBox(boundingBox), mParent(parent)
{
}

graphics::ScreenCoordinate IWindow::translatePosition(graphics::ScreenCoordinate coord)
{
  return coord - mBoundingBox.topLeft;
}

graphics::ScreenCoordinate IWindow::translatePosition(graphics::ScreenCoordinate coord, bool& inWindow)
{
  auto result(coord - mBoundingBox.topLeft);
  inWindow = result.x >= 0 && result.y >= 0 && result.x < size().x && result.y < size().y;
  return result;
}

bool IWindow::graphicsUpdateNeeded()
{
  return mDirty || (mDirty = checkForGraphicsChange());
}

void IWindow::drawBox(const graphics::Box& box, graphics::IGraphics::Color color, bool filled)
{
  mParent.drawBox({box.topLeft + mBoundingBox.topLeft, box.bottomRight + mBoundingBox.topLeft}, color, filled);
}

void IWindow::drawPolygon(const graphics::Polygon& pol,
  graphics::ScreenCoordinate position,
  Degree rotate,
  float scale,
  graphics::IGraphics::Color color,
  bool filled)
{
  mParent.drawPolygon(pol, position + mBoundingBox.topLeft, rotate, scale, color, filled);
}

bool IWindow::checkForGraphicsChange()
{
  return false;
}

graphics::ScreenCoordinate IWindow::size() const
{
  return mBoundingBox.bottomRight - mBoundingBox.topLeft;
}

void IWindow::dirty()
{
  mDirty = true;
}

void IWindow::clean()
{
  mDirty = false;
}

} // namespace ui
} // namespace gurgula
