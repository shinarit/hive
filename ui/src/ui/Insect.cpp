//
// Insect.cpp
//
//  Created on: 2018. aug. 30.
//      Author: fasz
//

#include <ui/Insect.hpp>

#include <ui/movement.hpp>

#include <algorithm>
#include <iterator>
#include <fstream>
#include <iostream>

namespace gurgula
{
namespace ui
{
::std::istream& operator>>(::std::istream& source, graphics::IGraphics::Color& color)
{
  int r, g, b;
  source >> r >> g >> b;
  color.red = r;
  color.green = g;
  color.blue = b;
  return source;
}

::std::istream& operator>>(::std::istream& source, Insect::BugShape::Piece& piece)
{
  int num;
  source >> num;
  for (int i(0); i < num; ++i)
  {
    graphics::ScreenCoordinate tempCoord;
    source >> tempCoord.x >> tempCoord.y;
    piece.polygon.push_back(tempCoord);
  }
  {
    char mode;
    source >> mode;
    switch (mode)
    {
      case 'F':
        piece.mode = Insect::BugShape::Piece::FILL;
        break;
      case 'O':
        piece.mode = Insect::BugShape::Piece::OUTLINE;
        break;
      case 'B':
        piece.mode = Insect::BugShape::Piece::BACKGROUND;
        break;
    }
  }
  return source;
}

::std::istream& operator>>(::std::istream& source, Insect::BugShape::Pieces& pieces)
{
  int num;
  source >> num;
  for (int i(0); i < num; ++i)
  {
    Insect::BugShape::Piece tempPiece;
    source >> tempPiece;
    pieces.push_back(tempPiece);
  }
  return source;
}

Insect::BugShape::BugShape(::std::istream& source)
{
  source >> color >> pieces;
}

namespace
{
class ShapeLoader
{
public:
  ShapeLoader()
  {
    {
      ::std::ifstream queenFile(QueenPath);
      new (queenShapeArea) Insect::BugShape(queenFile);
    }
  }

  const Insect::BugShape& queenShape() const
  {
    return *reinterpret_cast<const Insect::BugShape*>(&queenShapeArea[0]);
  }

private:
  static const char* const QueenPath;
  char queenShapeArea[sizeof(Insect::BugShape)];
};
const char* const ShapeLoader::QueenPath = "queen.pol";
} // namespace

Insect::BugShape Insect::getShape() const
{
  static ShapeLoader shapes;
  return shapes.queenShape();
  //  switch (species)
  //  {
  //    case Species::QUEEN_BEE:
  //      return QUEEN_BEE_SHAPE;
  //      //    case Species::BEETLE:
  //      //      return BEETLE_SHAPE;
  //      //    case Species::GRASSHOPPER:
  //      //      return GRASSHOPPER_SHAPE;
  //    default:
  //      return {{200, 250, 0}, {{BaseHex, Insect::BugShape::Piece::FILL}}};
  //  }
}

::std::vector<Insect::Path> Insect::getPaths(const HexCoordinate position, const GameBoard& board) const
{
  const auto neighbours(getNeighbours(position));

  if (!fulfillsOneHive(position, board))
  {
    return {};
  }

  typedef ::std::vector<Insect::Path> (*pathCalculator)(const HexCoordinate position, const GameBoard& board);
  const pathCalculator calculators[] = {queenBeePaths,
    spiderPaths,
    beetlePaths,
    grasshopperPaths,
    soldierAntPaths,
    mosquitoPaths,
    ladybugPaths,
    pillBugPaths};

  return calculators[static_cast<int>(board.find(position)->second.back().species)](position, board);
}

bool Insect::canPlace(Insect::Side side, HexCoordinate to, const GameBoard& board)
{
  if (board.size() < 2)
  {
    return true;
  }
  if (0 != board.count(to))
  {
    return false;
  }
  bool hasActualNeighbour(false);
  for (auto coord : getNeighbours(to))
  {
    auto neighbour(board.find(coord));
    if (board.end() != neighbour)
    {
      hasActualNeighbour = true;
      if (side != neighbour->second.back().side)
      {
        return false;
      }
    }
  }
  return hasActualNeighbour;
}

Insect::Targets Insect::collectPlacementTargets(const Side side, const GameBoard& board)
{
  if (0 == board.size())
  {
    return {{0, 0}};
  }
  if (1 == board.size())
  {
    auto starterNeighbours(getNeighbours({0, 0}));
    return Targets(begin(starterNeighbours), end(starterNeighbours));
  }

  Targets myNeighbours;
  Targets theirNeighbours;

  Targets* neighbourSelector[] = {&myNeighbours, &theirNeighbours};

  for (auto insect : board)
  {
    auto neighbours(getNeighbours(insect.first));
    neighbourSelector[side != insect.second.back().side]->insert(begin(neighbours), end(neighbours));
  }

  Targets result;
  ::std::set_difference(begin(myNeighbours),
    end(myNeighbours),
    begin(theirNeighbours),
    end(theirNeighbours),
    ::std::insert_iterator<Targets>(result, begin(result)));

  for (auto it(begin(result)); it != end(result);)
  {
    auto prevIt(it);
    ++it;
    if (end(board) != board.find(*prevIt))
    {
      result.erase(prevIt);
    }
  }

  return result;
}

Insect produceInsect(Insect::Side side, Insect::Species species)
{
  return Insect{side, species};
}

const char* nameSpecies(Insect::Species species)
{
  const char* translationTable[]
    = {"QUEEN_BEE", "SPIDER", "BEETLE", "GRASSHOPPER", "SOLDIER_ANT", "MOSQUITO", "LADYBUG", "PILL_BUG"};

  return translationTable[static_cast<int>(species)];
}

const char* nameSide(Insect::Side side)
{
  const char* translationTable[] = {"WHITE", "BLACK"};

  return translationTable[static_cast<int>(side)];
}

::std::ostream& operator<<(::std::ostream& out, const Insect& insect)
{
  out << nameSide(insect.side) << ' ' << nameSpecies(insect.species);
}

bool operator==(const Insect& lhs, const Insect& rhs)
{
  return lhs.side == rhs.side && lhs.species == rhs.species;
}

} // namespace ui
} // namespace gurgula
