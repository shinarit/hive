//
// movement.cpp
//
//  Created on: 2018. nov. 30.
//      Author: fasz
//

#include <ui/movement.hpp>

#include <algorithm>
#include <iterator>
#include <queue>

#include <common/Logger.hpp>

namespace gurgula
{
namespace ui
{

namespace
{
::std::vector<HexCoordinate> getActualNeighbours(const HexCoordinate position, const GameBoard& board)
{
  const auto neighbours(getNeighbours(position));

  // checking one hive rule
  ::std::vector<HexCoordinate> actualNeighbours;
  ::std::copy_if(begin(neighbours),
    end(neighbours),
    ::std::back_insert_iterator<::std::vector<HexCoordinate>>(actualNeighbours),
    [&board](const auto coord) { return end(board) != board.find(coord); });

  return actualNeighbours;
}

::std::vector<HexCoordinate> getFreeNeighbours(const HexCoordinate position, const GameBoard& board)
{
  const auto neighbours(getNeighbours(position));

  // checking one hive rule
  ::std::vector<HexCoordinate> freeNeighbours;
  ::std::copy_if(begin(neighbours),
    end(neighbours),
    ::std::back_insert_iterator<::std::vector<HexCoordinate>>(freeNeighbours),
    [&board](const auto coord) { return end(board) == board.find(coord); });

  return freeNeighbours;
}

::std::vector<bool> areNeighboursSlideable(const HexCoordinate position, const GameBoard& board)
{
  ::std::vector<bool> result(6);
  const auto oldNeighbours(getNeighbours(position));
  for (int i(0); i < oldNeighbours.size(); ++i)
  {
    auto noInsectOnPosition(end(board));
    bool blocked = (noInsectOnPosition != board.find(oldNeighbours[i])
      || (noInsectOnPosition != board.find(oldNeighbours[(i + 1) % 6])
           && noInsectOnPosition != board.find(oldNeighbours[(i + 5) % 6])));
    if (!blocked)
    {
      auto newNeighbours(getActualNeighbours(oldNeighbours[i], board));
      bool jumping = true;
      for (const auto insect : newNeighbours)
      {
        if (end(oldNeighbours) != ::std::find(begin(oldNeighbours), end(oldNeighbours), insect))
        {
          jumping = false;
          break;
        }
      }
      result[i] = !jumping;
    }
    else
    {
      result[i] = false;
    }
  }
  return result;
}

::std::vector<HexCoordinate> getSlideableNeighbours(const HexCoordinate position, const GameBoard& board)
{
  ::std::vector<HexCoordinate> result;
  const auto neighbours(getNeighbours(position));
  const auto slideability(areNeighboursSlideable(position, board));
  for (int i(0); i < neighbours.size(); ++i)
  {
    if (slideability[i])
    {
      result.push_back(neighbours[i]);
    }
  }
  return result;
}

::std::vector<Insect::Path> getSlideablePaths(const HexCoordinate position,
  const GameBoard& board,
  ::std::optional<int> maxDepth = ::std::optional<int>())
{
  GameBoard boardWithoutOriginalTile(board);
  boardWithoutOriginalTile.erase(position);
  ::std::vector<Insect::Path> result;

  auto neighbours(getSlideableNeighbours(position, boardWithoutOriginalTile));
  ::std::queue<Insect::Path> q;
  ::std::for_each(begin(neighbours), end(neighbours), [&q](const auto coord) { q.push({coord}); });
  ::std::unordered_set<HexCoordinate> destinations{position};

  while (!q.empty())
  {
    const auto currPath(q.front());
    q.pop();
    if (maxDepth.has_value())
    {
      if (*maxDepth == currPath.size())
      {
        result.push_back(currPath);
        //      destinations.insert(currPath.back());
        continue;
      }
    }
    else
    {
      result.push_back(currPath);
      destinations.insert(currPath.back());
    }

    neighbours = getSlideableNeighbours(currPath.back(), boardWithoutOriginalTile);
    neighbours.erase(::std::remove_if(begin(neighbours),
                       end(neighbours),
                       [&destinations, &currPath](const auto coord) {
                         return end(destinations) != destinations.find(coord)
                           || end(currPath) != ::std::find(begin(currPath), end(currPath), coord);
                       }),
      end(neighbours));

    for (const auto coord : neighbours)
    {
      auto nextPath(currPath);
      nextPath.push_back(coord);
      q.push(nextPath);
    }
  }

  return result;
}
} // namespace

bool fulfillsOneHive(const HexCoordinate position, const GameBoard& board)
{
  if (1 == board.at(position).size())
  {
    auto actualNeighbours(getActualNeighbours(position, board));

    if (1 < actualNeighbours.size())
    {
      ::std::unordered_set<HexCoordinate> reachable{position};
      ::std::queue<HexCoordinate> q{{actualNeighbours[0]}};
      while (!q.empty())
      {
        const auto nextCoord(q.front());
        q.pop();
        reachable.insert(nextCoord);
        const auto neighbours(getNeighbours(nextCoord));
        ::std::vector<HexCoordinate> actualNeighboursNotReachedYet;
        ::std::copy_if(begin(neighbours),
          end(neighbours),
          ::std::back_insert_iterator<::std::vector<HexCoordinate>>(actualNeighboursNotReachedYet),
          [&board, &reachable](
            const auto coord) { return end(reachable) == reachable.find(coord) && end(board) != board.find(coord); });
        reachable.insert(begin(actualNeighboursNotReachedYet), end(actualNeighboursNotReachedYet));
        ::std::for_each(begin(actualNeighboursNotReachedYet),
          end(actualNeighboursNotReachedYet),
          [&q](const auto coord) { q.push(coord); });
      }
      for (int i(1); i < actualNeighbours.size(); ++i)
      {
        if (0 == reachable.count(actualNeighbours[i]))
        {
          return false;
        }
      }
    }
  }
  return true;
}

::std::vector<Insect::Path> queenBeePaths(const HexCoordinate position, const GameBoard& board)
{
  ::std::vector<Insect::Path> result;
  const auto neighbours(getSlideableNeighbours(position, board));
  ::std::for_each(begin(neighbours), end(neighbours), [&result](const auto coord) { result.push_back({coord}); });
  return result;
}

::std::vector<Insect::Path> spiderPaths(const HexCoordinate position, const GameBoard& board)
{
  return getSlideablePaths(position, board, 3);
}

::std::vector<Insect::Path> beetlePaths(const HexCoordinate position, const GameBoard& board)
{
  // TODO beetle climbing freedom of movement rules
  ::std::vector<Insect::Path> result;
  if (1 < board.at(position).size())
  {
    // free range for the beetle
    const auto neighbours(getNeighbours(position));
    ::std::for_each(begin(neighbours), end(neighbours), [&result](const auto coord) { result.push_back({coord}); });
  }
  else
  {
    const auto slideableNeighbours(getSlideableNeighbours(position, board));
    const auto climbeableNeighbours(getActualNeighbours(position, board));
    ::std::for_each(
      begin(slideableNeighbours), end(slideableNeighbours), [&result](const auto coord) { result.push_back({coord}); });
    ::std::for_each(begin(climbeableNeighbours), end(climbeableNeighbours), [&result](const auto coord) {
      result.push_back({coord});
    });
  }
  return result;
}

::std::vector<Insect::Path> grasshopperPaths(const HexCoordinate position, const GameBoard& board)
{
  // we rely on neighbours being always in the same order
  ::std::vector<Insect::Path> result;
  auto originalNeighbours(getNeighbours(position));
  for (int i(0); i < originalNeighbours.size(); ++i)
  {
    if (end(board) != board.find(originalNeighbours[i]))
    {
      result.push_back({});
      auto neighbours(originalNeighbours);
      do
      {
        result.back().push_back(neighbours[i]);
        neighbours = getNeighbours(neighbours[i]);
      } while (end(board) != board.find(neighbours[i]));
      result.back().push_back(neighbours[i]);
    }
  }
  return result;
}

::std::vector<Insect::Path> soldierAntPaths(const HexCoordinate position, const GameBoard& board)
{
  return getSlideablePaths(position, board);
}

::std::vector<Insect::Path> mosquitoPaths(const HexCoordinate position, const GameBoard& board)
{
  return {};
}

::std::vector<Insect::Path> ladybugPaths(const HexCoordinate position, const GameBoard& board)
{
  return {};
}

::std::vector<Insect::Path> pillBugPaths(const HexCoordinate position, const GameBoard& board)
{
  return {};
}

} // namespace ui
} // namespace gurgula
