//
// PlayerWindow.cpp
//
//  Created on: 2018. aug. 22.
//      Author: fasz
//

#include <ui/PlayerWindow.hpp>

namespace gurgula
{
namespace ui
{
namespace
{
const graphics::ScreenCoordinate Margin{1, 1};
const int VerticalOffset = 80;
const int ClickDistance = 50;
const int ClickDistanceSquare = ClickDistance * ClickDistance;
const graphics::ScreenCoordinate MultipletOffset = {10, 2};
const graphics::IGraphics::Color WHITE_BACKGROUND_COLOR = {100, 150, 100};
const graphics::IGraphics::Color BLACK_BACKGROUND_COLOR = {100, 100, 150};
const double BASE_PIECE_ZOOM = 0.5;
} // namespace

PlayerWindow::PlayerWindow(IWindow& parent,
  const graphics::Box& boundingBox,
  Insect::Side side,
  GameConfig::InsectSet set)
  : IWindow(parent, boundingBox)
  , mPieceBaseColor(Insect::Side::BLACK == side ? BLACK_PIECE_BASE_COLOR : WHITE_PIECE_BASE_COLOR)
  , mBackColor(Insect::Side::BLACK == side ? BLACK_BACKGROUND_COLOR : WHITE_BACKGROUND_COLOR)
  , mLogger("RESERVE")
{
  for (const auto& setPiece : set)
  {
    if (0 < setPiece.second)
    {
      mRemainingInsects.push_back({produceInsect(side, setPiece.first), setPiece.second});
    }
  }
}

void PlayerWindow::updateGraphics()
{
  drawBox({Margin, size() - Margin}, mBackColor, true);

  int placedCount(0);
  for (const auto& setPiece : mRemainingInsects)
  {
    for (int i(setPiece.remaining - 1); i != -1; --i)
    {
      drawPolygon(BaseHex,
        positionForIndex(placedCount) + MultipletOffset * i,
        Degree(0),
        BASE_PIECE_ZOOM,
        mPieceBaseColor,
        true);
      drawPolygon(
        BaseHex, positionForIndex(placedCount) + MultipletOffset * i, Degree(0), BASE_PIECE_ZOOM, {255, 0, 0}, false);
      const auto bugShape(setPiece.insect.getShape());
      for (const auto& piece : bugShape.pieces)
      {
        drawPolygon(piece.polygon,
          positionForIndex(placedCount) + MultipletOffset * i,
          Degree(0),
          BASE_PIECE_ZOOM * PIECE_SIZE_RATION,
          Insect::BugShape::Piece::BACKGROUND == piece.mode ? mPieceBaseColor : bugShape.color,
          Insect::BugShape::Piece::OUTLINE != piece.mode);
      }
    }
    ++placedCount;
  }
  clean();
}

const Insect* PlayerWindow::hover(graphics::ScreenCoordinate coord) const
{
  for (int i(0); i < mRemainingInsects.size(); ++i)
  {
    if (ClickDistance > distance(coord, positionForIndex(i)))
    {
      return &mRemainingInsects[i].insect;
    }
  }
  return 0;
}

void PlayerWindow::removeInsect(Insect insect)
{
  for (int i(0); i < mRemainingInsects.size(); ++i)
  {
    if (insect == mRemainingInsects[i].insect)
    {
      mLogger() << "insect found at at " << i << ", " << mRemainingInsects[i].remaining << " remaining";
      if (0 == --mRemainingInsects[i].remaining)
      {
        mLogger() << "removing insect: " << mRemainingInsects[i].insect;
        mRemainingInsects.erase(mRemainingInsects.begin() + i);
      }
    }
  }
  dirty();
}

graphics::ScreenCoordinate PlayerWindow::positionForIndex(int i) const
{
  return {100, i * 2 * ClickDistance + VerticalOffset};
}

template<class T>
void saveSimpleData(const T& data, ::std::ofstream& out)
{
  out.write(reinterpret_cast<const char*>(&data), sizeof(T));
}

template<class T>
void loadSimpleData(T& data, ::std::ifstream& in)
{
  in.read(reinterpret_cast<char*>(&data), sizeof(T));
}

void PlayerWindow::save(::std::ofstream& out) const
{
  saveSimpleData(mRemainingInsects.size(), out);
  for (const auto piece : mRemainingInsects)
  {
    saveSimpleData(piece, out);
  }
}

void PlayerWindow::load(::std::ifstream& in)
{
  RemainingPieces::size_type newSize;
  loadSimpleData(newSize, in);
  mRemainingInsects.clear();
  for (int i(0); i < newSize; ++i)
  {
    RemainingPieces::value_type newValue;
    loadSimpleData(newValue, in);
    mRemainingInsects.push_back(newValue);
  }
}

} // namespace ui
} // namespace gurgula
