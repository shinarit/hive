//
// shaders.cpp
//
//  Created on: 2018. m�j. 8.
//      Author: fasz
//

#include <ui/GlWindow.hpp>

namespace gurgula
{
namespace ui
{

const char* GlWindow::sBoxVertexShader = "#version 330 core\n\
layout (location = 0) in vec2 aPos;\n\
void main()\n\
{\n\
  gl_Position = vec4(aPos.xy, 0, 1.0);\n\
}";

#define POSITION_UNI "objectPositionUniform"
#define ROTATION_UNI "rotationUniform"
#define SCALE_UNI "scaleUniform"
#define RESOLUTION_UNI "resolutionUniform"

const char* GlWindow::sPolygonVertexShader = "#version 330 core\n\
layout (location = 0) in vec2 vertexPos;\n\
uniform vec2 " POSITION_UNI ";\n\
uniform float " ROTATION_UNI ";\n\
uniform float " SCALE_UNI ";\n\
uniform vec2 " RESOLUTION_UNI ";\n\
mat2 rotateMatrix(float angle){\n\
  return mat2(cos(angle), -sin(angle),\n\
              sin(angle), cos(angle));\n\
}\n\
mat2 scaleMatrix(float scale){\n\
  return mat2(scale, 0.0,\n\
              0.0, scale);\n\
}\n\
vec2 pixelToOpengl(vec2 coord){\n\
  vec2 res = " RESOLUTION_UNI ";\n\
  return vec2((coord.x - res.x / 2) / res.x * 2, -(coord.y * 2 / res.y) + 1);\n\
}\n\
void main()\n\
{\n\
  vec2 transPos = scaleMatrix(" SCALE_UNI ") * rotateMatrix(" ROTATION_UNI ") * vertexPos;\n\
  transPos = transPos + " POSITION_UNI ";\n\
  gl_Position = vec4(pixelToOpengl(transPos), 0, 1.0);\n\
}";

#define COLOR_UNI "inColor"
const char* GlWindow::sColorShader = "#version 330 core\n\
out vec4 FragColor;\n\
uniform vec4 " COLOR_UNI ";\n\
void main()\n\
{\n\
    FragColor = " COLOR_UNI ";\n\
}";

const char* gColorUniformName = COLOR_UNI;
const char* gPositionUniformName = POSITION_UNI;
const char* gRotationUniformName = ROTATION_UNI;
const char* gScaleUniformName = SCALE_UNI;
const char* gResolutionUniformName = RESOLUTION_UNI;

} // namespace ui
} // namespace gurgula
