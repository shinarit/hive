//
// BoardWindow.cpp
//
//  Created on: 2018. aug. 29.
//      Author: fasz
//

#include <ui/BoardWindow.hpp>

#include <set>
#include <algorithm>
#include <cmath>
#include <optional>

namespace gurgula
{
namespace ui
{
namespace
{
const int BORDER = 20;
const double MAX_ZOOM = 2;

const graphics::IGraphics::Color BACKGROUND_COLOR = {0xff, 0xff, 0xff};
const graphics::IGraphics::Color SELECTION_TARGET_COLOR = {0, 200, 100};
const graphics::IGraphics::Color SECONDARY_TARGET_COLOR = {100, 250, 150};
const graphics::IGraphics::Color SELECTION_COLOR = {200, 200, 0};
const graphics::IGraphics::Color SECONDARY_SELECTION_COLOR = {250, 250, 100};
const graphics::IGraphics::Color BASE_HEX_COLOR = {200, 200, 200};
const graphics::IGraphics::Color TILE_OUTLINE_COLOR = {100, 100, 100};

const HexCoordinate BASE_STACK_OFFSET = {-5, -10};
const HexCoordinate EXTENDED_STACK_OFFSET = {-40, -80};

template<class CONT>
::gurgula::Rectangle<::gurgula::vector<int>> calculateBorders(const CONT& board)
{
  ::gurgula::Rectangle<::gurgula::vector<int>> result{{0, 0}, {0, 0}};
  for (auto coord : board)
  {
    if (coord.x < result.topLeft.x)
    {
      result.topLeft.x = coord.x;
    }
    if (coord.y < result.topLeft.y)
    {
      result.topLeft.y = coord.y;
    }
    if (coord.x > result.bottomRight.x)
    {
      result.bottomRight.x = coord.x;
    }
    if (coord.y > result.bottomRight.y)
    {
      result.bottomRight.y = coord.y;
    }
  }
  return result;
}

const Animation::Time STACK_LIFT_ANIMATION_TIME(200);
} // namespace

BoardWindow::BoardWindow(IWindow& parent, const graphics::Box& boundingBox)
  : IWindow(parent, boundingBox)
  , mCurrentZoom(MAX_ZOOM)
  , mCurrentCenter{0, 0}
  , mStackingAnimation(STACK_LIFT_ANIMATION_TIME)
  , mLogger("BOARD")
{
}

// bool BoardWindow::graphicsUpdateNeeded() const
//{
//  return true;
//}

void BoardWindow::updateGraphics(const GameBoard& board,
  ::std::optional<HexCoordinate> selection,
  Insect::Targets selectionTargets,
  ::std::optional<HexCoordinate> secondarySelection,
  Insect::Targets secondaryTargets,
  bool liftStacks)
{
  mStackingAnimation.ongoing();
  switch (mStackingAnimation.currentState())
  {
    case Animation::State::AT_START:
    {
      if (liftStacks)
      {
        mStackingAnimation.reStart();
      }
      break;
    }
    case Animation::State::GOING:
    {
      if (!liftStacks)
      {
        mStackingAnimation.reverse();
      }
      break;
    }
    case Animation::State::GOING_BACKWARDS:
    {
      if (liftStacks)
      {
        mStackingAnimation.reverse();
      }
      break;
    }
    case Animation::State::AT_END:
    {
      if (!liftStacks)
      {
        mStackingAnimation.reverse();
      }
      break;
    }
  }

  drawBox({{0, 0}, {size().x, size().y}}, BACKGROUND_COLOR, true);

  const double ZOOM_COEFFICIENT = 0.90;
  const double HIGHLIGHT_ZOOM_COEFFICIENT = 0.95;
  ::std::set<HexCoordinate> baseHexes;
  if (board.empty())
  {
    baseHexes.insert({0, 0});
  }
  else
  {
    for (const auto& tile : board)
    {
      const HexCoordinate& base(tile.first);
      for (const auto& neighbour : getNeighbours(base))
      {
        if (0 == board.count(neighbour))
        {
          baseHexes.insert(neighbour);
        }
      }
    }
  }
  auto boardBorders(calculateBorders(baseHexes));
  const int horizontalSpace(size().x - BORDER * 2);
  const int verticalSpace(size().y - BORDER * 2);
  const int boardWidth(boardBorders.bottomRight.x - boardBorders.topLeft.x + 1);
  const int boardHeight(boardBorders.bottomRight.y - boardBorders.topLeft.y + 1);
  double horizontalZoom(horizontalSpace
    / (boardWidth * BASE_HEX_HORIZONTAL_DISTANCE + 2 * BASE_HEX_HORIZONTAL_DISTANCE * BASE_HEX_HORIZONTAL_TRIM));
  double verticalZoom(verticalSpace / (boardHeight * BASE_HEX_VERTICAL_DISTANCE));
  mCurrentZoom = ::std::min(MAX_ZOOM, ::std::min(horizontalZoom, verticalZoom));
  mCurrentCenter = (boardBorders.topLeft + boardBorders.bottomRight);
  mCurrentCenter /= 2.0;
  for (auto coord : secondaryTargets)
  {
    drawPolygon(BaseHex,
      tilePlacement(coord),
      Degree(0),
      mCurrentZoom * HIGHLIGHT_ZOOM_COEFFICIENT,
      SECONDARY_TARGET_COLOR,
      true);
  }
  if (secondarySelection.has_value())
  {
    drawPolygon(BaseHex,
      tilePlacement(*secondarySelection),
      Degree(0),
      mCurrentZoom * HIGHLIGHT_ZOOM_COEFFICIENT,
      SECONDARY_SELECTION_COLOR,
      true);
  }
  for (auto coord : selectionTargets)
  {
    drawPolygon(BaseHex,
      tilePlacement(coord),
      Degree(0),
      mCurrentZoom * HIGHLIGHT_ZOOM_COEFFICIENT,
      SELECTION_TARGET_COLOR,
      true);
  }
  if (selection.has_value())
  {
    drawPolygon(
      BaseHex, tilePlacement(*selection), Degree(0), mCurrentZoom * HIGHLIGHT_ZOOM_COEFFICIENT, SELECTION_COLOR, true);
  }
  for (HexCoordinate coord : baseHexes)
  {
    drawPolygon(BaseHex, tilePlacement(coord), Degree(0), mCurrentZoom * ZOOM_COEFFICIENT, BASE_HEX_COLOR);
  }
  for (const auto& tile : board)
  {
    graphics::ScreenCoordinate position(tilePlacement(tile.first));
    int offsetIndex(0);
    for (const auto insect : tile.second)
    {
      const graphics::ScreenCoordinate offset(BASE_STACK_OFFSET * mCurrentZoom * offsetIndex
        + EXTENDED_STACK_OFFSET * mStackingAnimation.currentValue() * offsetIndex);
      const graphics::IGraphics::Color baseColor(
        Insect::Side::BLACK == insect.side ? BLACK_PIECE_BASE_COLOR : WHITE_PIECE_BASE_COLOR);
      drawPolygon(BaseHex, position + offset, Degree(0), mCurrentZoom * ZOOM_COEFFICIENT, baseColor, true);
      drawPolygon(BaseHex, position + offset, Degree(0), mCurrentZoom * ZOOM_COEFFICIENT, TILE_OUTLINE_COLOR, false);
      const auto bugShape(insect.getShape());
      for (const auto& piece : bugShape.pieces)
      {
        drawPolygon(piece.polygon,
          position + offset,
          Degree(0),
          mCurrentZoom * ZOOM_COEFFICIENT * PIECE_SIZE_RATION,
          Insect::BugShape::Piece::BACKGROUND == piece.mode ? baseColor : bugShape.color,
          Insect::BugShape::Piece::OUTLINE != piece.mode);
      }
      ++offsetIndex;
    }
  }
  clean();
}

HexCoordinate BoardWindow::boardCoordinates(graphics::ScreenCoordinate coord)
{
  const float DANGER_THRESHOLD = .32;
  coord -= size() / 2;
  coord.y = -coord.y;
  float column(coord.x / (BASE_HEX_HORIZONTAL_DISTANCE * mCurrentZoom) + mCurrentCenter.x);
  float row(coord.y / (BASE_HEX_VERTICAL_DISTANCE * mCurrentZoom) + mCurrentCenter.y
    - (::std::abs(static_cast<int>(::std::round(column))) % 2) * 0.5);
  HexCoordinate candidate{static_cast<int>(::std::round(column)), static_cast<int>(::std::round(row))};
  PreciseCoordinate delta{column - candidate.x, row - candidate.y};
  ::std::optional<Direction> otherCandidateDirection;
  if (delta.x > DANGER_THRESHOLD)
  {
    if (delta.y > 0)
    {
      otherCandidateDirection = Direction::NE;
    }
    else
    {
      otherCandidateDirection = Direction::SE;
    }
  }
  else if (delta.x < -DANGER_THRESHOLD)
  {
    if (delta.y > 0)
    {
      otherCandidateDirection = Direction::NW;
    }
    else
    {
      otherCandidateDirection = Direction::SW;
    }
  }
  if (otherCandidateDirection.has_value())
  {
    auto otherCandidate(getNeighbours(candidate)[static_cast<int>(otherCandidateDirection.value())]);
    if (distanceSqr(coord, tilePlacement(otherCandidate)) < distanceSqr(coord, tilePlacement(candidate)))
    {
      candidate = otherCandidate;
    }
  }
  // old, cube based algorithm
  //  coord -= size() / 2;
  //  double q((2. / 3 * coord.x) / (BASE_HEX_SIZE * mCurrentZoom));
  //  double r((-1. / 3 * coord.x + ::std::sqrt(3) / 3 * coord.y) / (BASE_HEX_SIZE * mCurrentZoom));
  //  auto result(cubeToFlatHex(cubeRound(q, -q - r, r)));
  //  mLogger() << q << ':' << r;
  //  mLogger() << "  res: " << result - mCurrentCenter;
  return candidate;
}

bool BoardWindow::checkForGraphicsChange()
{
  return mStackingAnimation.ongoing();
}

// algorithms taken from https://www.redblobgames.com/grids/hexagons/
BoardWindow::PreciseCoordinate BoardWindow::cubeToFlatHex(Cube cube)
{
  double col = cube.x;
  double row = cube.z + (cube.x + (static_cast<int>(cube.x) & 1)) / 2;
  return {static_cast<float>(col), static_cast<float>(row)};
}

BoardWindow::Cube BoardWindow::cubeRound(double x, double y, double z)
{
  double rx(::std::round(x));
  double ry(::std::round(y));
  double rz(::std::round(z));

  double xDiff(::std::abs(rx - x));
  double yDiff(::std::abs(ry - y));
  double zDiff(::std::abs(rz - z));

  if (xDiff > yDiff && xDiff > zDiff)
  {
    rx = -ry - rz;
  }
  else if (yDiff > zDiff)
  {
    ry = -rx - rz;
  }
  else
  {
    rz = -rx - ry;
  }

  return {rx, ry, rz};
}

graphics::ScreenCoordinate BoardWindow::tilePlacement(HexCoordinate coord)
{
  graphics::ScreenCoordinate center{size().x / 2, size().y / 2};
  PreciseCoordinate precCoord(coord);
  precCoord -= mCurrentCenter;

  return center
    + graphics::ScreenCoordinate{static_cast<int>(precCoord.x * BASE_HEX_HORIZONTAL_DISTANCE * mCurrentZoom),
        static_cast<int>(
          (-precCoord.y * BASE_HEX_VERTICAL_DISTANCE - ::std::abs(coord.x % 2) * BASE_HEX_VERTICAL_DISTANCE / 2)
          * mCurrentZoom)};
  // old, skewed angle algorithm
  //  return center
  //    + HexCoordinate{precCoord.x * BASE_HEX_HORIZONTAL_DISTANCE * mCurrentZoom,
  //        (precCoord.y * BASE_HEX_VERTICAL_DISTANCE + precCoord.x * BASE_HEX_VERTICAL_DISTANCE / 2) * mCurrentZoom};
}

} // namespace ui
} // namespace gurgula
