//
// GlWindow.cpp
//
//  Created on: 2018. �pr. 28.
//      Author: fasz
//

#include <ui/GlWindow.hpp>

#include <common/geometry.hpp>

#include <gl/glew.h>
#include <glfw/glfw3.h>

#include <algorithm>

namespace gurgula
{
namespace ui
{
extern const char* gColorUniformName;
extern const char* gPositionUniformName;
extern const char* gRotationUniformName;
extern const char* gScaleUniformName;
extern const char* gResolutionUniformName;

const IEventDistributor::KeyCode IEventDistributor::NamedCodes::S = GLFW_KEY_S;
const IEventDistributor::KeyCode IEventDistributor::NamedCodes::L = GLFW_KEY_L;
const IEventDistributor::KeyCode IEventDistributor::NamedCodes::K = GLFW_KEY_K;

bool GlWindow::GL_INITIALIZED = false;

GlWindow::GlWindow(Mode mode, WindowCoordinate size): mLogger("GL")
{
  if (!GL_INITIALIZED)
  {
    if (GL_FALSE == glfwInit())
    {
      mLogger() << "error initializing glfw";
      throw PhysicalWindowException();
    }
  }

  createWindow(mode, size);

  if (!GL_INITIALIZED)
  {
    GLenum err(glewInit());
    if (GLEW_OK != err)
    {
      mLogger() << "error initializing glew: " << glewGetErrorString(err);
      throw PhysicalWindowException();
    }
    GlWindow::GL_INITIALIZED = true;
  }

  init();
}

GlWindow::~GlWindow()
{
  glfwDestroyWindow(mWindow);
}

// IGraphics
graphics::ScreenCoordinate GlWindow::size() const
{
  return mSize;
}

void GlWindow::drawBox(const graphics::Box& box, Color color, bool filled) const
{
  glPolygonMode(GL_FRONT_AND_BACK, filled ? GL_FILL : GL_LINE);
  glUseProgram(mBoxShaderProgram);
  glBindVertexArray(mBoxVao);

  OpenGlScreenCoordinate topLeft(translatePixelToDrawScreenCoordinate(box.topLeft));
  OpenGlScreenCoordinate botRight(translatePixelToDrawScreenCoordinate(box.bottomRight));
  float vertices[] = {static_cast<float>(topLeft.x),
    static_cast<float>(topLeft.y),
    static_cast<float>(topLeft.x),
    static_cast<float>(botRight.y),
    static_cast<float>(botRight.x),
    static_cast<float>(botRight.y),
    static_cast<float>(botRight.x),
    static_cast<float>(topLeft.y)};
  int vertexColorLocation = glGetUniformLocation(mBoxShaderProgram, gColorUniformName);
  glUniform4f(vertexColorLocation, color.red / 255., color.green / 255., color.blue / 255., 1.0f);

  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glDrawArrays(GL_QUADS, 0, 4);
}

void GlWindow::drawPolygon(const graphics::Polygon& pol,
  graphics::ScreenCoordinate position,
  Degree rotate,
  float scale,
  Color color,
  bool filled) const
{
  glPolygonMode(GL_FRONT_AND_BACK, filled ? GL_FILL : GL_LINE);
  glUseProgram(mPolygonShaderProgram);
  glBindVertexArray(mPolygonVao);

  int uniformLocation(glGetUniformLocation(mPolygonShaderProgram, gColorUniformName));
  glUniform4f(uniformLocation, color.red / 255., color.green / 255., color.blue / 255., 1.0f);
  uniformLocation = glGetUniformLocation(mPolygonShaderProgram, gPositionUniformName);
  glUniform2f(uniformLocation, position.x, position.y);
  uniformLocation = glGetUniformLocation(mPolygonShaderProgram, gRotationUniformName);
  glUniform1f(uniformLocation, degreeToRadian(rotate).value);
  uniformLocation = glGetUniformLocation(mPolygonShaderProgram, gScaleUniformName);
  glUniform1f(uniformLocation, scale);
  uniformLocation = glGetUniformLocation(mPolygonShaderProgram, gResolutionUniformName);
  glUniform2f(uniformLocation, mSize.x, mSize.y);

  float vertices[MAX_POLYGON_LENGTH * 2];
  for (int i(0); i < ::std::min<int>(pol.size(), MAX_POLYGON_LENGTH); ++i)
  {
    vertices[2 * i] = pol[i].x;
    vertices[2 * i + 1] = pol[i].y;
  }
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pol.size() * 2, vertices, GL_STATIC_DRAW);

  //  glDrawArrays(GL_LINE_LOOP, 0, pol.size());
  glDrawArrays(GL_POLYGON, 0, pol.size());
}

bool GlWindow::wantsToDie() const
{
  return glfwWindowShouldClose(mWindow);
}

IPhysicalWindow::Mode GlWindow::getMode() const
{
  return mMode;
}

bool GlWindow::changeMode(Mode mode)
{
  if (mode == mMode)
  {
    return true;
  }
  glfwDestroyWindow(mWindow);
  createWindow(mode, mSize);
  init();
  return mMode == mode;
}

IPhysicalWindow::WindowCoordinate GlWindow::getSize() const
{
  return mSize;
}

bool GlWindow::setSize(WindowCoordinate size)
{
  if (Mode::BORDERLESS_WINDOW == mMode)
  {
    return false;
  }
  glfwSetWindowSize(mWindow, size.x, size.y);
  WindowCoordinate oldSize(mSize);
  glfwGetWindowSize(mWindow, &mSize.x, &mSize.y);
  return mSize != oldSize;
}

void GlWindow::startUpdate()
{
  // auto oldSize(mSize);
  // TODO window resize callbacks maybe?
  glfwGetWindowSize(mWindow, &mSize.x, &mSize.y);
  glViewport(0, 0, mSize.x, mSize.y);
}

void GlWindow::finishUpdate()
{
  glfwSwapBuffers(mWindow);
}

GlWindow::CallbackId GlWindow::registerMouseButtonEventHandler(MouseButtonId id, MouseButtonEventCallback callback)
{
  mMouseButtonEventHandlers[id].push_back({mCallbackIdGenerator.nextId(), callback});
}

GlWindow::CallbackId GlWindow::registerMouseMoveEventHandler(MouseMoveEventCallback callback)
{
  mMouseMoveEventHandlers[mCallbackIdGenerator.nextId()] = callback;
}

GlWindow::CallbackId GlWindow::registerKeyboardEventHandler(KeyCode id, KeyboardEventCallback callback)
{
  mKeyboardEventHandlers[id].push_back({mCallbackIdGenerator.nextId(), callback});
}

void GlWindow::unregisterCallback(CallbackId id)
{
}

void GlWindow::handleEvents()
{
  glfwPollEvents();
}

void GlWindow::init()
{
  // create the shader programs
  char infoLog[512];
  int success;

  unsigned int boxVertexShader;
  boxVertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(boxVertexShader, 1, &sBoxVertexShader, 0);
  glCompileShader(boxVertexShader);
  glGetShaderiv(boxVertexShader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(boxVertexShader, 512, NULL, infoLog);
    mLogger() << "shader compilation failed:" << infoLog << '\n' << sBoxVertexShader;
    throw PhysicalWindowException();
  }

  unsigned int polygonVertexShader;
  polygonVertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(polygonVertexShader, 1, &sPolygonVertexShader, 0);
  glCompileShader(polygonVertexShader);
  glGetShaderiv(polygonVertexShader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(polygonVertexShader, 512, NULL, infoLog);
    mLogger() << "shader compilation failed:" << infoLog << '\n' << sPolygonVertexShader;
    throw PhysicalWindowException();
  }

  unsigned int fragmentShader;
  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &sColorShader, 0);
  glCompileShader(fragmentShader);
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    mLogger() << "shader compilation failed:" << infoLog << '\n' << sColorShader;
    throw PhysicalWindowException();
  }

  mBoxShaderProgram = glCreateProgram();
  glAttachShader(mBoxShaderProgram, boxVertexShader);
  glAttachShader(mBoxShaderProgram, fragmentShader);
  glLinkProgram(mBoxShaderProgram);

  mPolygonShaderProgram = glCreateProgram();
  glAttachShader(mPolygonShaderProgram, polygonVertexShader);
  glAttachShader(mPolygonShaderProgram, fragmentShader);
  glLinkProgram(mPolygonShaderProgram);

  glDeleteShader(boxVertexShader);
  glDeleteShader(polygonVertexShader);
  glDeleteShader(fragmentShader);

  glGetProgramiv(mBoxShaderProgram, GL_LINK_STATUS, &success);
  if (!success)
  {
    glGetProgramInfoLog(mBoxShaderProgram, 512, NULL, infoLog);
    mLogger() << "shader linking failed:" << infoLog;
    throw PhysicalWindowException();
  }

  // box VAO
  glGenVertexArrays(1, &mBoxVao);
  glBindVertexArray(mBoxVao);

  // box VBO
  unsigned int VBO;
  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);

  // polygon VAO
  glGenVertexArrays(1, &mPolygonVao);
  glBindVertexArray(mPolygonVao);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);
}

void GlWindow::createWindow(Mode mode, WindowCoordinate size)
{
  mMode = mode;
  mSize = size;
  glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);

  GLFWmonitor* monitor(glfwGetPrimaryMonitor());
  if (Mode::BORDERLESS_WINDOW == mode)
  {
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

    const GLFWvidmode* vidMode(glfwGetVideoMode(monitor));
    mSize = {vidMode->width, vidMode->height};
  }
  else if (Mode::BORDERED_WINDOW == mode)
  {
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
  }

  mWindow = glfwCreateWindow(mSize.x, mSize.y, "PENIS", Mode::EXCLUSIVE_FULLSCREEN == mode ? monitor : 0, 0);
  if (!mWindow)
  {
    throw - 2;
  }

  glfwGetWindowSize(mWindow, &mSize.x, &mSize.y);

  glfwMakeContextCurrent(mWindow);

  glfwSwapInterval(1);
  mCallbackHandler.registerHandlers(*this);
}

void GlWindow::keyCallback(int key, int scancode, int action, int mods)
{
  if (GLFW_REPEAT != action)
  {
    auto& handlers(mKeyboardEventHandlers[key]);
    ::std::for_each(begin(handlers), end(handlers), [key, action](auto& callbackObject) {
      callbackObject.callback({key, ((GLFW_PRESS == action) ? ButtonStatus::PRESSED : ButtonStatus::RELEASED)});
    });
  }
}

void GlWindow::cursorPositionCallback(double xpos, double ypos)
{
  ::std::for_each(begin(mMouseMoveEventHandlers), end(mMouseMoveEventHandlers), [xpos, ypos](auto& mapObject) {
    mapObject.second({{xpos, ypos}});
  });
}

void GlWindow::mouseButtonCallback(int button, int action, int mods)
{
  auto& handlers(mMouseButtonEventHandlers[button]);
  ::std::for_each(begin(handlers), end(handlers), [button, action](auto& callbackObject) {
    callbackObject.callback({button, ((GLFW_PRESS == action) ? ButtonStatus::PRESSED : ButtonStatus::RELEASED)});
  });
}

GlWindow::OpenGlScreenCoordinate GlWindow::translatePixelToDrawScreenCoordinate(graphics::ScreenCoordinate coord) const
{
  return {float(coord.x - mSize.x / 2) / mSize.x * 2, -(float(coord.y * 2. / mSize.y) - 1)};
}

//////////////////////////////
////// CallbackHandler ///////
//////////////////////////////

GlWindow* GlWindow::CallbackHandler::sWindow = 0;

GlWindow::CallbackHandler::CallbackHandler()
{
}

void GlWindow::CallbackHandler::registerHandlers(GlWindow& window)
{
  sWindow = &window;
  glfwSetKeyCallback(sWindow->mWindow, keyCallback);
  glfwSetCursorPosCallback(sWindow->mWindow, cursorPositionCallback);
  glfwSetMouseButtonCallback(sWindow->mWindow, mouseButtonCallback);
}

void GlWindow::CallbackHandler::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  sWindow->keyCallback(key, scancode, action, mods);
}

void GlWindow::CallbackHandler::cursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
  sWindow->cursorPositionCallback(xpos, ypos);
}

void GlWindow::CallbackHandler::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
  sWindow->mouseButtonCallback(button, action, mods);
}

} // namespace ui
} // namespace gurgula
