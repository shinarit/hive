//
// animation.cpp
//
//  Created on: 2018. dec. 3.
//      Author: fasz
//

#include <ui/Animation.hpp>

#include <common/Logger.hpp>

namespace gurgula
{
namespace ui
{
using ::std::chrono::steady_clock;

Animation::Animation(Time length): mCurrentState(State::AT_START), mLength(length)
{
}

bool Animation::ongoing()
{
  if (State::GOING == mCurrentState || State::GOING_BACKWARDS == mCurrentState)
  {
    const auto now(steady_clock::now());
    if (mEndTime <= now)
    {
      if (State::GOING == mCurrentState)
      {
        mCurrentState = State::AT_END;
      }
      else
      {
        mCurrentState = State::AT_START;
      }
      return false;
    }
    return true;
  }
  return false;
}

Animation::AnimationValue Animation::currentValue() const
{
  switch (mCurrentState)
  {
    case State::AT_START:
      return 0;
    case State::GOING:
      return calculateValue();
    case State::GOING_BACKWARDS:
      return 1 - calculateValue();
    case State::AT_END:
      return 1;
  }
}

Animation::State Animation::currentState() const
{
  return mCurrentState;
}

void Animation::reStart()
{
  mStartTime = steady_clock::now();
  mEndTime = mStartTime + mLength;

  mCurrentState = State::GOING;
}

void Animation::reverse()
{
  if (!ongoing())
  {
    reStart();
    mCurrentState = State::GOING_BACKWARDS;
  }
  else
  {
    const auto now(steady_clock::now());
    auto oldStart(mStartTime);
    auto oldEnd(mEndTime);
    mStartTime = now - (mEndTime - now);
    mEndTime = now + (now - oldStart);
    switchDirection();
  }
}

Animation::AnimationValue Animation::calculateValue() const
{
  const auto now(steady_clock::now());
  return static_cast<AnimationValue>((now - mStartTime).count()) / (mEndTime - mStartTime).count();
}

void Animation::switchDirection()
{
  if (State::GOING == mCurrentState)
  {
    mCurrentState = State::GOING_BACKWARDS;
  }
  else if (State::GOING_BACKWARDS == mCurrentState)
  {
    mCurrentState = State::GOING;
  }
}

} // namespace ui
} // namespace gurgula
