//
// BugCommon.cpp
//
//  Created on: 2018. szept. 2.
//      Author: fasz
//

#include <ui/BugCommon.hpp>

namespace gurgula
{
namespace ui
{
const HexCoordinate EvenSteps[] = {{0, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}};
const HexCoordinate OddSteps[] = {{0, 1}, {1, 1}, {1, 0}, {0, -1}, {-1, 0}, {-1, 1}};

Neighbours getNeighbours(HexCoordinate coord)
{
  Neighbours result;
  decltype(EvenSteps)& steps(0 == (coord.x % 2) ? EvenSteps : OddSteps);
  int i(0);
  for (auto step : steps)
  {
    result[i++] = coord + step;
  }
  return result;
}

} // namespace ui
} // namespace gurgula
